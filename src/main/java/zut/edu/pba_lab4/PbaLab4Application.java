package zut.edu.pba_lab4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PbaLab4Application {

	public static void main(String[] args) {
		SpringApplication.run(PbaLab4Application.class, args);
	}

}
