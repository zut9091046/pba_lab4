package zut.edu.pba_lab4;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import zut.edu.api.UsersApi;
import zut.edu.model.CreateRequest;
import zut.edu.model.UpdateRequest;
import zut.edu.model.UserListResponse;
import zut.edu.model.UserResponse;

import java.util.UUID;

@Controller
public class UserController implements UsersApi {

    @Override
    public ResponseEntity<UserResponse> createUser(CreateRequest body) {
        return null;
    }

    @Override
    public ResponseEntity<Void> deleteUser(UUID id) {
        return null;
    }

    @Override
    public ResponseEntity<UserListResponse> getAllUsers() {
        System.out.println("working!!!");
        return null;
    }

    @Override
    public ResponseEntity<UserResponse> getUserById(UUID id) {
        return null;
    }

    @Override
    public ResponseEntity<UserResponse> updateUser(UUID id, UpdateRequest body) {
        return null;
    }
}
